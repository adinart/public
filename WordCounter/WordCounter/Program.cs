﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WordCounter
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Part 1
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "fileinput.txt");
            string text = System.IO.File.ReadAllText(path);
            var countedWords = WordCounter(text);

            Console.WriteLine("Word\t\tCount");
            countedWords.OrderBy(w => w.Word).ToList().ForEach(c => Console.WriteLine("{0}\t\t{1}", c.Word, c.Count));
            Console.WriteLine();
            Console.WriteLine("Press any key to use filter . . .");            
            Console.ReadLine();

            // Part 2
            IEnumerable<string> listWords = WordCounterWithFilter(text);
            string stringWords = string.Join(", ", listWords.OrderBy(w => w).ToArray());
           
            Console.WriteLine(stringWords);
            Console.WriteLine();

            Console.WriteLine("Press any key to continue . . .");
            Console.ReadLine();
        }

        public static IEnumerable<CountedWord> WordCounter(string text)
        {         
            // replace all white space with space
            text = Regex.Replace(text, @"\s+", " ");

            // remove all punctuation marks
            text = Regex.Replace(text, @"[^\w\s]", "");

            string[] words = text.ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var groupedWords = words.GroupBy(w => w.Trim());            
            var countedWords = groupedWords.Select(group => new CountedWord() { Word = group.Key, Count = group.Count() });

            return countedWords;
        }

        public static IEnumerable<string> WordCounterWithFilter(string text)
        {
            var words = WordCounter(text).Select(x => x.Word);

            // get all words with count = 6
            var sixCharWords = words.Where(x => x.Length == 6);

            // loop through 6 
            // if match any 1 - 5 word, 
            //      if the remaining word match any 1 - 5 - put in list

            List<string> listWords = new List<string>();
            sixCharWords.ToList().ForEach(sixCharWord =>
            {
                for (int i = 1; i <= 5; i++)
                {
                    var firstWordList = words.Where(x => x.Length == i);
                    firstWordList.ToList().ForEach(firstWord =>
                    {
                        var idx1 = sixCharWord.IndexOf(firstWord, 0, i);
                        if (idx1 > -1)
                        {
                            var secondWordList = words.Where(x => x.Length == (6 - i));
                            secondWordList.ToList().ForEach(secondWord =>
                            {
                                var idx2 = sixCharWord.IndexOf(secondWord, i);
                                if (idx2 > -1)
                                {
                                    listWords.Add(sixCharWord);
                                }
                            });
                        }
                    });
                }

            });

            return listWords.Distinct();
        }
    }

    public class CountedWord
    {
        public string Word { get; set; }
        public int Count { get; set; }
    }
}
