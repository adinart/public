﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordCounter;

namespace WordCounter.Tests
{
    [TestClass]
    public class ProgramTests
    {
        string inputtext;
        List<string> expectedOutput;

        // To do: Some tests for part 1, for example testing the number of word count for text that contains white space

        [TestInitialize]
        public void Setup()
        {
            inputtext = "al, albums, aver, bar, barely, be, befoul, bums, by, cat, con, convex, ely, foul, here, hereby, jig, jigsaw, or, saw, tail, tailor, vex, we, weaver";
            expectedOutput = new List<string>() { "albums", "barely", "befoul", "convex", "hereby", "jigsaw", "tailor", "weaver" };
        }

        [TestMethod]
        public void WordCounterWithFilter_ValidInput_EqualNumberOfOutput()
        {
            // arrange            

            // act
            var testOutput = Program.WordCounterWithFilter(inputtext);

            // assert
            Assert.AreEqual(testOutput.Count(), expectedOutput.Count());
        }

        [TestMethod]
        public void WordCounterWithFilter_ValidInput_CorrectOutputItems()
        {
            // arrange

            // act
            var testOutput = Program.WordCounterWithFilter(inputtext);

            // assert
            testOutput.ToList().ForEach(o => Assert.IsTrue(expectedOutput.Contains(o)));           
        }
    }
}
